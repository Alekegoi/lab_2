# frozen_string_literal: true

# Qwerty qwerty
class Grade
  attr_reader :number, :date
  def initialize(number, date)
    @number = number
    @date = date
  end

  def to_s
    "#{@number} on #{@date}"
  end
end

# sdf
Grade
# asd
