# frozen_string_literal: true

# sdd
class Student
  attr_accessor :name
  def initialize(name)
    @name = name
  end

  def to_s
    @name
  end
end
